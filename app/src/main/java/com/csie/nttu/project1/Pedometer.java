package com.csie.nttu.project1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;


/**
 * Created by bo on 2016/8/14.
 */
public class Pedometer {
    //表格名稱
    public static final String TABLE_NAME = "step_counter";

    // 編號表格欄位名稱，固定不變
    public static final String KEY_ID = "_id";

    // 其它表格欄位名稱
    public static final String DATETIME_COLUMN = "datetime";
    public static final String COUNT_COLUMN = "count";

    // 使用上面宣告的變數建立表格的SQL指令
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "  ( " +
            KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DATETIME_COLUMN + " INTEGER NOT NULL, " +
            COUNT_COLUMN + " INTEGER NOT NULL)";

    //資料庫物件
    private SQLiteDatabase db;

    //建構子
    public Pedometer(Context context) {
        db = MyDBHelper.getDatabase(context);
    }

    //關閉資料庫
    public void close() {
        db.close();
        //db.deleteDatabase("mydata.db");
    }

    //新增參數指定的物件
    public Item insert(Item item) {
        //建立準備新增資料的ContentValues物件
        ContentValues cv = new ContentValues();

        //加入ContentValues物件包裝的新增資料
        //第一個參數是欄位名稱，第二個參數是欄位的資料
        cv.put(DATETIME_COLUMN, item.getDatetime());
        cv.put(COUNT_COLUMN, item.getSteps());

        //新增一筆資料並取得編號
        long id = db.insert(TABLE_NAME, null, cv);

        item.setId(id);
        return item;
    }

    //修改參數指定的物件
    public boolean update(Item item) {
        //建立準備修改資料的ContentValues物件
        ContentValues cv = new ContentValues();

        //加入ContentValues物件包裝的新增資料
        //第一個參數是欄位名稱，第二個參數是欄位的資料
        cv.put(DATETIME_COLUMN, item.getDatetime());
        cv.put(COUNT_COLUMN, item.getSteps());

        //設定修改資料的條件為編號
        //格式為欄位名稱=資料
        String where = KEY_ID + "=" + item.getId();

        //執行修改資料並回傳修改的資料數量是否成功
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

//    //刪除參數指定編號的資料
//    public boolean delete(long id) {
//        //設定條件為編號，格式為欄位名稱=資料
//        String where = KEY_ID + "=" + id;
//        //刪除指定編號資料並回傳刪除是否成功
//        return db.delete(TABLE_NAME, where, null) > 0;
//    }
//
    // 讀取所有記事資料
    public Cursor getAll() {

        Cursor cursor = db.rawQuery(TABLE_NAME, null);

        return cursor;
    }

//    // 取得指定編號的資料物件
//    public Item get(long id) {
//        // 準備回傳結果用的物件
//        Item item = null;
//        // 使用編號為查詢條件
//        String where = KEY_ID + "=" + id;
//        // 執行查詢
//        Cursor result = db.query(
//                TABLE_NAME, null, where, null, null, null, null, null);
//
//        // 如果有查詢結果
//        if (result.moveToFirst()) {
//            // 讀取包裝一筆資料的物件
//            item = getRecord(result);
//            }
//
//        // 關閉Cursor物件
//        result.close();
//        // 回傳結果
//        return item;
//    }
//
//    // 把Cursor目前的資料包裝為物件
//    public Item getRecord(Cursor cursor) {
//        // 準備回傳結果用的物件
//        Item result = new Item();
//
//        result.setId(cursor.getLong(0));
//        result.setDatetime(cursor.getLong(1));
//        result.setColor(ItemActivity.getColors(cursor.getInt(2)));
//        result.setTitle(cursor.getString(3));
//        result.setContent(cursor.getString(4));
//        result.setFileName(cursor.getString(5));
//        result.setLatitude(cursor.getDouble(6));
//        result.setLongitude(cursor.getDouble(7));
//        result.setLastModify(cursor.getLong(8));
//
//        // 回傳結果
//        return result;
//    }
//
    // 取得資料數量
    public int getCount() {
        int result = 0;
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);

        if (cursor.moveToNext()) {
            result = cursor.getInt(0);
        }

        return result;
    }

    // 建立範例資料
    public void sample() {
        Item item = new Item(0, new Date().getTime(), 0);
        Item item2 = new Item(0, new Date().getTime(), 100);
        insert(item);
        insert(item2);
    }
}
