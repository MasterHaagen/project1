package com.csie.nttu.project1;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;


import java.util.Date;
import java.util.List;

/**
 * Created by Sjokz on 2016/8/31.
 */

public class StepCounter implements SensorEventListener {
   /* private TextView textViewX;
    private Button btnStart;*/
    private SensorManager mSensorManager;
    private List<Sensor> slist;
    private Sensor mAccelerometer;
    float[] x = new float[100];
    float[] y = new float[100];
    float[] z = new float[100];
    float gx = 0.0f;
    float gy = 0.0f;
    boolean dw = false;
    boolean fw = false;
    boolean going = false;
    String strx = "";
    String stry = "";
    String strz = "";
    int count = 0;
    int step = 0;
    boolean is_start = false;
    int time_count;
    private Handler mHandlerTime = new Handler();


    protected void onCreate(Bundle savedInstanceState) {
        /*super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);*/
        //textViewX =(TextView)findViewById(R.id.textViewX);
       // Button start = (Button)findViewById(R.id.btnStart);
       // start.setOnClickListener(startfirst);
       // mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer  = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.unregisterListener(this);
        mHandlerTime.postDelayed(timerRun,1000);
        mHandlerTime.removeCallbacks(timerRun);
    }
    private final Runnable timerRun = new Runnable(){
        public void run(){
            mHandlerTime.postDelayed(this, 100);
        }
    };
   /* public void onDestroy(){
        mHandlerTime.removeCallbacks(timerRun);
        super.onDestroy();
    }*/
@Override
    public void onSensorChanged(SensorEvent event) {
        if(count == 0){
            x[0] = event.values[0];
            y[0] = event.values[1];
            //z[count] = event.values[2];
            gx = x[0]-x[1];
            gy = y[0]-y[1];
            count = 1;
        }else{
            x[1] = event.values[0];
            y[1] = event.values[1];
            //z[count] = event.values[2];
            gx = x[1]-x[0];
            gy = y[1]-y[0];
            count = 0;
        }


        if(gx > 0.5)
            dw = true;
        else if (gx < -0.5)
            dw = false;

        if(gy < -0.5)
            fw = true;
        else if (gy > 0.5)
            fw = false;

        if(dw && fw) going = true;

        if(going && !dw && !fw){
            step++;
            going = false;
        }

        //textViewX.setText("Step: "+step);
		/*count++;
		if(count >=20) {
			for(int i=0;i<=count;i++){
				strx += " + "+x[i];
				stry += " + "+y[i];
				strz += " + "+z[i];
			}
			textViewX.setText(	"X: "+strx+"\n"+
								"Y: "+stry+"\n"+
								"Z: "+strz+"\n");
			onPause();
		}*/
    }
    /*OnClickListener startfirst = new OnClickListener(){
        @Override
        public void onClick(View v){
            if(is_start) restart(); else onResume();
            //textViewX.setText("Start");
        }
    };*/
    protected void onResume() {
//        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        x = new float[100];
        y = new float[100];
        z = new float[100];
        gx = 0.0f;
        gy = 0.0f;
        dw = false;
        fw = false;
        going = false;
        count = 0;
        step = 0;
        strx = "";
        stry = "";
        strz = "";
        is_start = true;
        timerRun.run();
    }
   /* protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        onDestroy();
        is_start = false;
    }
    protected void restart(){
        onPause();
        onResume();

    }*/

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }
}
